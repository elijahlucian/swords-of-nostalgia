"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
// 0, 1, 2, 3 = n, e, s, w (like css)

var gameTile = exports.gameTile = function gameTile() {
    return {
        id: "gameTileId",
        exits: ["gameTileId", "gameTileId", null, null], // exits in every direction
        items: ["uid", "uid", "uid"], // spawnItems(),
        mobs: ["uid", "uid"] // spawnMobs()
    };
};