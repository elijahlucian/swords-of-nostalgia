"use strict";

var _classes = require("./classes");

// swords of nostalgia

// initialize crap

var userInput = process.openStdin();

// classes

// game methods

var initializeWorld = function initializeWorld() {
    return createGameTile(0, 0, null);
};

var createGameTile = function createGameTile(playerX, playerY, origin_tile_id) {
    return new _classes.GameTile();
};

var checkMovement = function checkMovement(direction) {
    console.log("moving to the", direction);
    // check map for available GameTile
    // if false createGameTile()
    // if true MoveToGameTile
};

var checkEquipment = function checkEquipment() {
    console.log('you are carrying a rusty dagger');
};

var lookAround = function lookAround() {
    console.log("you are in a clearing.");
    // list possible directions
    console.log("you see a path leading north...");
    console.log("[go] north?");
    // list items and monsters
};

// async user input listener

userInput.addListener('data', function (d) {
    var ui = d.toString().trim();

    // main game controls
    switch (ui.split(' ')[0]) {
        case 'go':
            checkMovement(ui.split(' ')[1]);
            break;
        case 'exit':
            console.log("exiting program...");
            process.exit();
            break;
        case 'look':
            lookAround('');

            break;
        case 'hands':
            checkEquipment();
            break;
        default:
            break;
    }

    process.stdout.write('>> ');
});

var playerX = 0;
var playerY = 0;

console.log('initializing world...');

var worldMap = initializeWorld(playerX, playerY);

console.log(worldMap);

console.log('you have materialized into an unknown world, what do you do?');
console.log('[look] around?');
process.stdout.write('>> ');