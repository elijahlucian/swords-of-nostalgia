'use strict';

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Health = function Health(props) {
    _classCallCheck(this, Health);

    this.health = props.health;
};

var Player = function (_Health) {
    _inherits(Player, _Health);

    function Player(props) {
        _classCallCheck(this, Player);

        var _this = _possibleConstructorReturn(this, (Player.__proto__ || Object.getPrototypeOf(Player)).call(this, props));

        _this.state = {
            equipment: {
                right: 'rusty dagger',
                left: 'empty'
            },
            backpack: [],
            gold: 0
        };
        return _this;
    }

    return Player;
}(Health);

var Weapon = function (_Health2) {
    _inherits(Weapon, _Health2);

    function Weapon(props) {
        _classCallCheck(this, Weapon);

        var _this2 = _possibleConstructorReturn(this, (Weapon.__proto__ || Object.getPrototypeOf(Weapon)).call(this, props));

        _this2.name = props.name;
        return _this2;
    }

    return Weapon;
}(Health);

var Item = function Item() {
    _classCallCheck(this, Item);

    this.state = {};
};

var Quest = function Quest() {
    _classCallCheck(this, Quest);

    this.state = {};
};

var Enemy = function Enemy() {
    _classCallCheck(this, Enemy);

    this.state = {};
};

var GameTile = function GameTile() {
    _classCallCheck(this, GameTile);

    // randomize items

    this.state = {
        items: [],
        enemies: []
    };
};