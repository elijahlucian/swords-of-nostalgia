// 0, 1, 2, 3 = n, e, s, w (like css)

export const gameTile = () => {
    return {
        id: "gameTileId",
        exits: ["gameTileId","gameTileId",null,null], // exits in every direction
        items: ["uid", "uid", "uid"], // spawnItems(),
        mobs: ["uid", "uid"] // spawnMobs()
    }
}
