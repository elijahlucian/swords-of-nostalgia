
class Health {
    constructor(props) {
        this.health = props.health
    }
}

class Player extends Health {
    constructor(props) {
        super(props)

        this.state = {
            equipment: {
                right: 'rusty dagger',
                left: 'empty'
            },
            backpack: [],
            gold: 0
        }
    }
}

class Weapon extends Health{
    constructor(props) {
        super(props)
        this.name = props.name
    }
}

class Item {
    constructor() {
        this.state = {}
    }
}

class Quest {
    constructor() {
        this.state = {}
    }
}

class Enemy {
    constructor() {
        this.state = {}
    }
}

class GameTile {
    constructor(props) {

        // randomize items
        this.state = {
            items: [],
            enemies: []
        }
    }
}
