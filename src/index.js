// swords of nostalgia

// initialize crap

var worldMap = {
    tiles: []
}
currentTileId = -1
var userInput = process.openStdin()

// classes

// import { Health, Player, Weapon, Item, Quest, Enemy, GameTile } from "./classes";

class Health {
    constructor(props) {
        this.health = props.health
    }
}

class Player extends Health {
    constructor(props) {
        super(props)

        this.state = {
            equipment: {
                right: new Weapon({kind: init, name: "rusty dagger"}),
                left: null
            },
            backpack: [],
            gold: 0
        }
    }
}

class Weapon extends Health{
    constructor(props) {
        super(props)
        if(props.kind === 'init'){
            // create initial weapon
        }
        this.name = props.name
    }
}

class Spell {
    constructor(props){
        this.kind = "default spell"
    }
}

class Item {
    constructor() {
        this.state = {}
    }
}

class Quest {
    constructor() {
        this.state = {}
    }
}

class Enemy {
    constructor() {
        this.state = {}
    }
}

class GameTile {
    constructor(props) {
        
        // randomize exits
        // randomize items
        // randomize mobs
        
        this.id = props.id
        this.exits = [true,false,false,false] // true means there is an option to move, but it is undiscovered, false = no exit
        this.items = []
        this.enemies = []
    }
}

// game methods

const initializeWorld = () => {
    return createGameTile()
    // worldMap.tiles 
}

const createGameTile = (currentId, key) => {
    currentTileId += 1
    worldMap[currentTileId] = new GameTile({id: "random_uuid"})
}

const checkMovement = (direction) => {

    const validateDirection = (key) => {
        if(worldMap[currentTileId].exits[key] === true) {
            // if direction = true, then create a new game tile and move to it.
            let gt = createGameTile(currentTileId, key)
            currentTileId = worldMap[currentTileId].id

        } else if ( worldMap[currentTileId].exits[key] ) {
            // if there is a uuid, move to that game time
        } else {
            console.log("you cannot go that way...")
        }
    }

    switch (direction) {
        case "north":
            validateDirection(0)
            break;
        case "east":
            validateDirection(1)
            break;
        case "south":
            validateDirection(2)
            break;
        case "west":
            validateDirection(3)
            break;
        default:
            console.log("that is not a valid direction")
            return
            break;
    }

    console.log("moving to the", direction)
    // check map for available GameTile
    // if false createGameTile()
    // if true MoveToGameTile
}

const checkEquipment = () => {
    // let equip = player.equipmentLeft, player.equipmentRight
    console.log('you are carrying a rusty dagger')
}

const lookAround = () => {
    console.log("you are in a clearing.")
    // list possible directions
    console.log("you see a path leading north...")
    console.log("[go] north?")
    // list items and monsters
}

// async user input listener

userInput.addListener('data', (d) => {
    let ui = d.toString().trim()

    // main game controls
    switch (ui.split(' ')[0]) {
        case 'go':
            checkMovement(ui.split(' ')[1])
            break
        case 'exit':
            console.log("exiting program...")
            process.exit()
            break;
        case 'look':
            lookAround('')
            
            break
        case 'hands':
            checkEquipment()
            break
        default:
            break;
    }

    process.stdout.write('>> ')

})

console.log('initializing world...')

initializeWorld()

// worldMap = {
//     tiles: [...worldMap.tiles, initializeWorld()]
// }

console.log(worldMap)

console.log('you have materialized into an unknown world, what do you do?')
console.log('[look] around?')
process.stdout.write('>> ')